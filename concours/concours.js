function listeDePrix(points, age){
	if(age < 12 || age > 100 || points < 0 || points > 100){ 
		return "invalide";
	}
	else{ 
		liste = []
		if(points >= 50){ 
			liste.push("bon");
		}
		if(age < 15){ 
			liste.push("invitation");
		}
		if(age >= 20 && age <= 30 && points >= 80){ 
			liste.push("bourse");
		}
		if(age < 18 && points >= 60){ 
			liste.push("cadeau");
		}
		if(age > 30 && points >= 90){ 
			liste.push("réduction");
		}
		if(points >= 95){ 
			liste.push("médaille");
		}
		if(points >= 30 && points < 50){ 
			liste.push("encouragement");
		}
		return liste.sort(); // pour avoir un ordre alphabétique
	}
}

export { listeDePrix };
